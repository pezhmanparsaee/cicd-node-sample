import http from "http";

const server = http.createServer(function(req, res) {
  res.statusCode = 200;
  res.setHeader("content-type", "text/plain");
  res.end("Hi there");
});

server.listen(3000, () => {
  console.log("App is up and running on port 3000");
});
